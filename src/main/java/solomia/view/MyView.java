package solomia.view;

import solomia.model.Droid;
import solomia.model.Ship;

import java.io.*;
import java.lang.reflect.Constructor;
import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.nio.ByteBuffer;
import java.nio.channels.FileChannel;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Scanner;

public class MyView {
    //private static Logger logger1= (Logger) LogManager.getLogger(MyView.class);
    private Map<String, String> menu;
    private Map<String, Printable> methodsMenu;
    private static Scanner input = new Scanner(System.in);

    public MyView() {
        menu = new LinkedHashMap<>();
        menu.put("1", "  1 - Serialization");
        menu.put("2", "  2 - Usual reader from 1.txt");
        menu.put("3", "  3 - Buffered reader from 1.txt");
        menu.put("4", "  4 - Contents of a specific directory");
        menu.put("5", "  5 - NIO example");
        menu.put("Q", "  Q - exit");

        methodsMenu = new LinkedHashMap<>();
        methodsMenu.put("1", this::pressButton1);
        methodsMenu.put("2", this::pressButton2);
        methodsMenu.put("3", this::pressButton3);
        methodsMenu.put("4", this::pressButton4);
        methodsMenu.put("5", this::pressButton5);

    }

    private void pressButton5() throws IOException {
        RandomAccessFile aFile =
                new RandomAccessFile("1.txt", "rw");
        FileChannel inChannel = aFile.getChannel();

//create buffer with capacity of 48 bytes
        ByteBuffer buf = ByteBuffer.allocate(48);

        int bytesRead = inChannel.read(buf); //write into buffer.
        while (bytesRead != -1) {

            buf.flip();  //make buffer ready for read from

            while (buf.hasRemaining()){
                System.out.print((char) buf.get()); // read 1 byte at a time
            }

            buf.clear(); //make buffer ready for writing into
            bytesRead = inChannel.read(buf);
        }

        aFile.close();

    }

    private void pressButton4() {
        System.out.println("contents of a specific directory");
        File file = new File("D:\\Other");
        if (file.exists()) {
            printDirectory(file, "");
        } else {
            System.out.println("Directory does not exist");
        }
    }

    private void printDirectory(File file, String s) {
        System.out.println(s + "Directory ");
        s = s + " ";
        File []fileName = file.listFiles();
        for (File f: fileName) {
            if (f.isDirectory()) {
                printDirectory(f, s);
            } else {
                System.out.println(s+" File "+ f.getName());
            }
        }
    }

    private void pressButton3() throws IOException {
        int buf = 1 * 1024 * 1024; // can without it (1 MB buffer)
        int count = 0;
        System.out.println("Test buffered reader");
        DataInputStream in = new DataInputStream(new BufferedInputStream(new FileInputStream("1.txt"), buf));
        while (in.available() > 0) {
            byte b = in.readByte();
            count++;
        }
        in.close();
        System.out.println("count = " + count);
    }

    private void pressButton2() throws IOException {
        int count = 0;
        System.out.println("Test usual reader");
        InputStream inputStream = new FileInputStream("1.txt");
        int data = inputStream.read();
        while (data != -1) {
            data = inputStream.read();
            count++;
        }
        inputStream.close();
        System.out.println("count = " + count);
    }


    private void pressButton1() {
        Ship.value = 90;
        Ship ship = new Ship("Cosmo-32", new Droid("Droid1", 123), 12);
        System.out.println(ship);
        try {
            ObjectOutputStream out = new ObjectOutputStream(new FileOutputStream("Ship.dat"));
            out.writeObject(ship);
            out.close();
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
        //Ship.value=100;
        try {
            ObjectInputStream in = new ObjectInputStream(new FileInputStream("Ship.dat"));
            Ship ship1 = (Ship) in.readObject();
            in.close();
            System.out.println(ship1);
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        }
    }

    public void show() {
        String keyMenu;
        do {
            outputMenu();
            System.out.println("Please, select menu point.");
            keyMenu = input.nextLine().toUpperCase();
            try {
                methodsMenu.get(keyMenu).printGoods();
            } catch (Exception e) {
            }
        } while (!keyMenu.equals("Q"));
    }

    private void outputMenu() {
        System.out.println("\nMENU:");
        for (String str : menu.values()) {
            System.out.println(str);
        }
    }

}