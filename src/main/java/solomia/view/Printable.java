package solomia.view;

import java.io.IOException;

@FunctionalInterface
public interface Printable {
    void printGoods() throws IOException;
}
