package solomia.model;

import java.io.Serializable;

public class Droid implements Serializable {
    private String name;
    int strength;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getStrength() {
        return strength;
    }

    public void setStrength(int strength) {
        this.strength = strength;
    }

    public Droid(String name, int strength) {
        this.name = name;
        this.strength = strength;
    }

    @Override
    public String toString() {
        return "Droid{" +
                "name='" + name + '\'' +
                ", strength=" + strength +
                '}';
    }
}
